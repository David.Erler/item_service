## requirements:
- docker v 18.9
- docker-compose*
- docker swarm**

## Build
without testing(requires no database)
```bash
DOCKER_BUILDKIT=1 docker build --progress plain -t item_service:latest .
```
with a local pg docker instance
```bash
docker run --rm --name pg-docker -e POSTGRES_PASSWORD=docker -d -p 127.0.0.1:5000-5100:5432 postgres
export PGPORT=$(docker port pg-docker | grep -E '127.0.0.1:[0-9]{4}' -o | grep -E '[0-9]{4}' -o)
DOCKER_BUILDKIT=1 docker build --network host --build-arg DBPORT=$PGPORT --progress plain --target test -t item_service:test .
DOCKER_BUILDKIT=1 docker build --progress plain -t item_service:latest .
docker kill pg-docker
```

## Develop
```bash
docker network create pg-docker
docker run --rm --name pg-docker -e POSTGRES_PASSWORD=docker -d --network pg-docker postgres
DOCKER_BUILDKIT=1 docker build --progress plain --target alpine_base -t item_service:dev .
ocker run -it -u root -v $(pwd):/workspace -w /workspace -e DBHOST=pg-docker -e DBPORT=5432 -e DBPASSWORD=docker -e RAILS_ENV=test --network pg-docker item_service:dev /bin/sh

## rake db:create
## rake db:migrate
## rake test
```
The sources are shared between the host system and the docker container. Changes on the host can be tested directly inside the docker container.


## Build and Test DIND
```bash
docker run --privileged --name docker-builder -d docker:dind
docker run --rm --link docker-builder:docker docker /bin/sh -c "docker -H docker network create test-db && docker -H docker run --rm -d --name test-db --network test-db -e POSTGRES_PASSWORD=docker postgres"
docker run --rm --link docker-builder:docker -v $(pwd):/workspace -w /workspace docker:dind /bin/sh -c "docker -H docker build --network test-db --build-arg DBHOST=test-db --build-arg DBPASSWORD=docker ."
docker run --rm --link docker-builder:docker docker /bin/sh -c "docker -H docker kill test-db && docker -H docker network rm test-db"
docker kill docker-builder && docker container rm docker-builder
```

## Build and Test DinDonD
```bash
docker run --rm -v /var/run/docker.sock:/var/run/docker.sock docker /bin/sh -c "docker network create test-db && docker run --rm -d --name test-db --network test-db -e POSTGRES_PASSWORD=docker postgres"
docker run --rm -v /var/run/docker.sock:/var/run/docker.sock -v $(pwd):/workspace -w /workspace docker:dind /bin/sh -c "docker build -t item_service:latest --network test-db --build-arg DBHOST=test-db --build-arg DBPASSWORD=docker ."
docker run --rm -v /var/run/docker.sock:/var/run/docker.sock docker /bin/sh -c "docker kill test-db && docker network rm test-db"
```
## run local postgres instance

```bash
docker pull postgres
mkdir -p $HOME/docker/volumes/postgres
docker network create pg-docker
docker run --rm --network pg-docker --name pg-docker -e POSTGRES_PASSWORD=<DBPASSWORD> -d -p 5432:5432 -v $HOME/docker/volumes/postgres:/var/lib/postgresql/data  postgres
```
## build, test and run the image with your local postgres instance
The Docker build creates the database, runs the migrations and executes all tests on your local pg instance.
```bash
docker build -t item_service:latest --network pg-docker .
docker run -e DBHOST=pg-docker -e DBPASSWORD=<DBPASSWORD> -p 3000:3000 --network pg-docker item_service:latest
```

## Docker Swarm Mode
**Currently not supported**  
requirements:
- a running traefik instance in swarm mode
- an external db network(overlay and swarmmode)
- a running pg database, which has the service name db and is connected to the db network
```bash
docker network create -d overlay --scope swarm db
docker network create -d overlay --scope swarm traefik
docker stack deploy -c ./docker-stack-db.yml item_service_db
docker stack deploy -c ./docker-stack-traefik.yml item_service_traefik
docker stack deploy -c ./docker-stack.yml item_service
```

## Docker Swarm Mode(Standalone)
```bash
docker stack deploy -c ./docker-stack-standalone.yml item_service_standalone
```