FROM ruby:2.6-alpine AS alpine_base
RUN apk add --update alpine-sdk postgresql-dev tzdata
# Create a group and user
RUN addgroup -S itemservice && adduser -S itemservice -G itemservice \
    && mkdir /application \
    && chown itemservice /application /home/itemservice
USER itemservice
WORKDIR /application
# utilize the build cache by installing the gems before adding the application code
# bundle install runs only when the Gemfile gets updated or the build cache is not present(e.g.
# --no-cache option is set or there are no previous build present).
COPY Gemfile Gemfile.lock /application/
RUN bundle install --without development --jobs=8

# add the application source code
COPY --chown=itemservice:itemservice . /application

FROM ruby:2.6 AS base
# Install required packages
# RUN apt-get update -qq && apt-get install -y --no-install-recommends nodejs postgresql-client && apt-get clean
# Create a non root user to run the application and set permissions
RUN groupadd -r itemservice && useradd --no-log-init -r -g itemservice itemservice
RUN mkdir /application
RUN mkdir /home/itemservice
RUN chown itemservice /application /home/itemservice
USER itemservice
WORKDIR /application
# utilize the build cache by installing the gems before adding the application code
# bundle install runs only when the Gemfile gets updated or the build cache is not present(e.g.
# --no-cache option is set or there are no previous build present).
COPY Gemfile /application/Gemfile
COPY Gemfile.lock /application/Gemfile.lock
RUN bundle install --jobs=8 

# add the application source code
COPY --chown=itemservice:itemservice . /application

FROM alpine_base AS test
ARG DBHOST=localhost
ARG DBPORT=5432
ARG DBPASSWORD=docker
ENV RAILS_ENV=test

# RUN if [ "$TEST" == "false" ]; then exit 0; fi \
# RUN curl $DBHOST:$DBPORT
# create the DB
RUN rake db:create \
# run the migration
    && rake db:migrate \
# run the test suite
    && rake test

FROM alpine_base
USER itemservice
ENV RAILS_ENV=production
# Start the main process. Try to create db and run migrations before starting the rails server
CMD ["/bin/sh", "-c", "rake db:create && rake db:migrate && rails server -b 0.0.0.0"]